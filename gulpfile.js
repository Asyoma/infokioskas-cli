
const exec = require('child_process').exec;
const gulp = require('gulp'); 
const fs = require("fs");
const rimraf = require("rimraf");
const path = require('path');
const rename = require("gulp-rename");


function pjson(){
let rawdata = fs.readFileSync('./package.json');
return JSON.parse(rawdata);
}

gulp.task('build', function (cb) {
    const display_name = pjson().display_name;
    const name = pjson().name;
    const version = pjson().version;

    const fileName_win= display_name+"_"+version+".exe";
    const fileName_linux = display_name+"_"+version+".run";

    
    const action = async ()=>{

        await gulp.src("./build/"+name+"-linux")
        .pipe(rename(fileName_linux))
        .pipe(gulp.dest("./build"));

        await gulp.src("./build/"+name+"-win.exe")
        .pipe(rename(fileName_win))
        .pipe(gulp.dest("./build"));

        setTimeout(()=>{
            fs.unlink("./build/"+name+"-win.exe",()=>{
                fs.unlink("./build/"+name+"-linux",()=>{
    
                    cb();
                })
            });
        },1000);
    }

    exec("gulp make",(err,stdout,stderr)=>{
        rimraf("./build",  ()=> { 
            console.log("Cleared ./build");
            fs.mkdir("./build",()=>{
                exec("pkg .",(err,stdout,stderr)=>{
                    action();
                });
            });
            
            
        });
    });

    

});

/** */


gulp.task('make', function (cb) {
    const action = ()=>{
        exec("tsc",function(err,stdout,stderr){
            console.log(stdout);
            console.error(stderr);
            
    
            cb();
        });
    }
    
    if(!fs.existsSync("./dist")){
        fs.mkdirSync("./dist");
        console.log("Created ./dist");
        action();
    }
    else{
        rimraf("./dist", function () { 
            console.log("Cleared ./dist");
            fs.mkdirSync("./dist");
            action();
         });
        
    }
    

   
})

gulp.task('release', function(cb){
    const display_name = pjson().display_name;
    const name = pjson().name;
    const version = pjson().version;



    const release_file = (file,type,platform,version,t_cb)=>{
          if(fs.existsSync("./build/"+file)){
              const currentDir = process.cwd();
              const cmd = 'curl -F "application=@'+currentDir+'/build/'+file+'" https://node.infokiosk.lt/app/upload/'+name+'/'+platform+'/'+type+'/'+version;
              exec(cmd,function(err,stdout,stderr){
                  console.log(stdout);
                  console.error(stderr);
                  t_cb();
              });
          }
          else{
              t_cb();
          }
          
    }

    const release_linux = (t_cb)=>{
          const filename = display_name+"_"+version+".run";
          release_file(filename,"run","linux",version,()=>{
              t_cb();
          });
    }
    
    const release_windows = (t_cb)=>{
        const filename = display_name+"_"+version+".exe";
        release_file(filename,"exe","windows",version,()=>{
          t_cb();
      });
    }

    
      release_windows(()=>{
        release_linux(()=>{
            cb();
          });
      });
      cb();
    
  
});


