import * as os from "os";
import axios from 'axios';
import { createWriteStream, writeFile,unlink } from "fs";
import * as path from "path";
import {LINE}from "./line";
import { Platforms, Types } from "./enums";


export class Network{
    platform:Platforms;
    type:Types;
    constructor(t_type:Types=Types.NULL,t_platform:Platforms=Platforms.NULL){
        if(t_type == Types.NULL){
            switch(os.platform()){
                case "win32":
                    this.type = Types.EXE;
                break;
                default:
                    this.type = Types.AppImage;
                break;
            }
        }else{
            this.type = t_type;
        }

        if(t_platform == Platforms.NULL){
            switch(os.platform()){
                case "win32":
                    this.platform = Platforms.WINDOWS;
                break;
                default:
                    this.platform = Platforms.LINUX;
                break;
            }
        }else{
            this.platform = t_platform;
        }
    }
    

    downloadFile = /*async */(id:string,fileName: string,appFolder:string,cb:any) =>{
        console.log('Downloading "'+id+'" > '+fileName);
        console.log(LINE);
        
        const fileUrl = "https://node.infokiosk.lt/app/download/"+id+"/"+this.platform+"/"+fileName;
        //const outputLocationPath = path.join(appFolder,fileName);
        const Downloader = require('nodejs-file-downloader');
        (async () => {

            const downloader = new Downloader({
              url: fileUrl,//If the file name already exists, a new file with the name 200MB1.zip is created.     
              directory: appFolder,//This folder will be created, if it doesn't exist. 
              fileName: fileName,
              cloneFiles:false    
            })
            try {
              await downloader.download();//Downloader.download() returns a promise.
        
              cb(true);
            } catch (error) {//IMPORTANT: Handle a possible error. An error is thrown in case of network errors, or status codes of 400 and above.
              //Note that if the maxAttempts is set to higher than 1, the error is thrown only if all attempts fail.
              console.log('Download failed',error)
              cb(false);
            }
        
        
        })();  
    }

    getLatestJson(app_id:string,type:Types=Types.NULL){
        let t_type = this.type;
        if(type != Types.NULL){
            t_type = type;
        }
        const url = 'https://node.infokiosk.lt/app/latest/'+app_id+'/'+this.platform+'/'+t_type;
        console.log(url);
        const r = axios.get(url)
        .then((response) => {
            if(response.status == 200 && response.data.status == true){
                return response.data.data;
            }else{
                return null;
            }
        });
        return r;
        
    }

    async getLatestJsonDual(app_id:string){
        const url = 'https://node.infokiosk.lt/app/latest/'+app_id+'/'+this.platform;
        console.log(url);
        const r = await axios.get(url)
        .then((response) => {
            if(response.status == 200 && response.data.status == true){
                return response.data.data;
            }else{
                return null;
            }
        }).catch(e=>{
            return null;
        });
        return r;
        
        
    }

    async getAppsList(){

        const url = 'https://node.infokiosk.lt/app/list';
        
        const d = axios.get(url)
        .then((response) => {
            if(response.status == 200 && response.data.status == true){
                return response.data.list;
            }else{
                return null;
            }
        });
        return d;
        
    }
}