import { Platforms, Types } from "./enums";

interface Params{
    platform:Platforms,
    type:Types,
    list:boolean,
    add:{
        status:boolean,
        param:string
    },
    remove:{
        status:boolean,
        param:string
    }
}

interface ServerObject{
    platform: Platforms,
    filename: string,
    timestamp: number,
    version: string,
    type: Types,
    appName: string,
    
}

interface LocalObject extends ServerObject{
    keepRunning:boolean
}

interface CheckingResultedObjects{
    old:LocalObject,
    new:LocalObject,
    needUpdate:boolean,
    canContinue:boolean
}



export{
    Params,
    ServerObject,
    LocalObject,
    CheckingResultedObjects
}