import * as ping from "ping";

const PingDefaults = (cb) =>{
    Ping("1.1.1.1",(res_1111)=>{
        //console.log"1.1.1.1= "+JSON.stringify(res_1111));
        Ping("google.com",(res_google)=>{
            //console.log"google.com = "+JSON.stringify(res_google));
            if(res_1111.alive == true && res_google.alive == true){
                cb(true);
            }
            else{
                cb(false);
            }
        });
    });

} 

const Ping = (host,cb) =>{
    (async () => {
        cb(await ping.promise.probe(host, {
            timeout: 10
        }));
    })();
        
}

export {
    PingDefaults,
    Ping
};