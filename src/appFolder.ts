import * as path from "path";
import * as os from "os";
import * as fs from "fs";
export class AppFolder{
    appRootPath:string;
    constructor(cb:any){
        this.appRootPath = path.join(os.homedir(),"Documents","InfokioskasApps");
        const checkAppPath = () =>{
            if(!fs.existsSync(this.getAppsPath())){
                fs.mkdirSync(this.getAppsPath());
                setTimeout(() => {
                    cb();
                }, 300);
            }
            else{
                setTimeout(() => {
                    cb();
                }, 300);
            }
        }
        
        if(!fs.existsSync(this.appRootPath)){
            fs.mkdirSync(this.appRootPath);
            checkAppPath();
        }
        else{
            checkAppPath();
        }
        
    }

    getRootPath(){
        return this.appRootPath;
    }

    getAppsPath(){
        return path.join(this.appRootPath,"Apps");
    }



}