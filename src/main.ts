import { Params } from "./interfaces";
import { AppFolder } from "./appFolder";
import { Settings } from "./settings";
import { App } from "./app";
import {LINE}from "./line";
import { OutputToConsole } from "./outputToConsole";
import * as cron from 'node-cron';
import { PingDefaults } from "./system";
export class Main{
    outputToConsole:OutputToConsole;
    appFolder: AppFolder;
    settings: Settings;
    params_obj:Params;
    running_app_list:string[]=[];
    exec_app_list:string[]=[];
    full_app_list_item:string[]=[];
    isChecking=false;
    isOnline=false;
    constructor(obj:Params=null){
        this.params_obj = obj;
        this.appFolder = new AppFolder(()=>{
            
            setTimeout(()=>{
                this.settings = new Settings(this.appFolder.getRootPath(),()=>{
                    if(!this.settings.hasSetting("list")){
                        this.settings.addValue("list","its-kiosk-browser");
                        this.settings.addValue("list","its-kiosk-autoreset");
                        this.settings.addValue("list","its-kiosk-browser-pdf-viewer");
                        this.settings.addValue("list","its-kiosk-browser-monitoring");
                        this.settings.addValue("list","its-kiosk-browser-remote-screensaver");
                    }
                    console.log(LINE);
                    console.log("Welcome to Infokioskas CLI");
                    setTimeout(()=>{
                        //
                        this.outputToConsole = new OutputToConsole(this.settings,this.params_obj,()=>{
                            setTimeout(()=>{
                                this.init();
                            },300)
                           
                        });
                    },100)
                    
                });
                
            },100);
            
        });
        
    }

    init(){
        if(this.params_obj.list == false && this.params_obj.add.status == false && this.params_obj.remove.status == false){
            const cronJob = cron.schedule('*/5 * * * * *', () => {
                if(this.isOnline == false && this.isChecking == false){
                    this.isChecking = true;
                    PingDefaults(res_internet_stat=>{
                        this.isChecking = false;
                        this.isOnline = res_internet_stat;
                        if(res_internet_stat == true){
                            this.isOnline = true;
                            cronJob.stop();
                            this.initApps();
                            
                        }
                        
                    })
                }
            });
            
        }else if(this.params_obj.list == true){
            this.outputToConsole.listApps(()=>{
                process.exit(0);
            });
        }
        else if(this.params_obj.add.status == true && this.params_obj.remove.status == true){
            this.outputToConsole.addAndRemove(()=>{
                process.exit(0);
            });
        }
        else if(this.params_obj.add.status == true ){
            this.outputToConsole.addApp(()=>{
                process.exit(0);
            });
        } 
        else if(this.params_obj.remove.status == true ){
            this.outputToConsole.removeApp(()=>{
                process.exit(0);
            });
        }
        else{
            process.exit(0);
        }

       
    }

    private initApps(){
        console.log(LINE);
        console.log('Starting updater');
        console.log(LINE);

        (async ()=>{
            const a_list = await this.settings.getSetting('list');
            console.log("Working with Apps list:");
            console.log(LINE);
            a_list.forEach((app:string) => {
                console.log("  -  "+app);
                new App(app,this.settings,this.params_obj);
                if(a_list.indexOf(app) == a_list.length-1){
                    console.log(LINE);
                    console.log("End of list.");
                    console.log(LINE);
                }
            });
        })();

    }

}