
enum Platforms{
    NULL = "null",
    LINUX = "linux",
    WINDOWS = "windows",
    CROSS_PLATFORM = "cross-platform"
}
enum Types{
    NULL = "null",
    ARM64 = "arm64",
    ARM32 = "arm32",
    AppImage = "appimage",
    RUN = "run",
    EXE = "exe",
    ZIP = "zip",
    EXEC_ZIP = "exec-zip"
}

export{
    Platforms,
    Types
}