import { CheckingResultedObjects, Params } from "./interfaces";
import { Settings } from "./settings";
import { CheckAppFolder } from "./AppModules/checkAppFolder";
import { DownloadLastestApp } from "./AppModules/downloadLatestApp";
import { ValidateAppSettings } from "./AppModules/validateAppSettings";
import { RunApp } from "./AppModules/runApp";

export class App{
    app_id:string;
    settings:Settings;
    appFolder;
    params_obj:Params;
    runTimes:number=0;
    constructor(id:string,settings:Settings,params_obj:Params){
        this.app_id = id;
        this.params_obj=params_obj;
        this.settings = settings;
        setTimeout(()=>{this.init();},300)
    }

    private init(){
        if(this.runTimes <=3){
            this.runTimes = this.runTimes + 1;
            console.log("BEGIN Checking App folder");
            new CheckAppFolder(this.app_id,(checkAppFolder_stat,a_folder_path)=>{
                console.log("END Checking App folder");
                console.log("VALUE Checking App folder");
                console.log("checkAppFolder_stat = ",checkAppFolder_stat);
                console.log("a_folder_path = ",a_folder_path);
                console.log("BEGIN ValidateAppSettings");
                new ValidateAppSettings(this.app_id,this.settings,a_folder_path,(objs:CheckingResultedObjects)=>{
                    console.log("END ValidateAppSettings");
                    console.log("VALUE ValidateAppSettings");
                    console.log("ValidateAppSettings = ",objs);

                    if(objs.canContinue == true){
                        if(checkAppFolder_stat == true){
                            if(objs.needUpdate == false){

                                new RunApp(objs.old,a_folder_path,(isOK:boolean)=>{
                                    if(isOK == false){
                                        this.init();
                                    }
                                });
                            }
                            else{
                                new DownloadLastestApp(objs.new,a_folder_path,(download_stat)=>{
                                    if(download_stat == true){
                                        this.settings.setSetting("apps."+this.app_id,objs.new);
                                        new RunApp(objs.new,a_folder_path,(isOK:boolean)=>{
                                            if(isOK == false){
                                                this.init()
                                            }
                                        });
                                    }
                                    else{
                                        this.init();
                                    }
                                    
                                });
                                
                            }
                            
                        }
                        else{
                            new DownloadLastestApp(objs.new,a_folder_path,(download_stat)=>{
                                if(download_stat == true){
                                    this.settings.setSetting("apps."+this.app_id,objs.new);
                                    new RunApp(objs.new,a_folder_path,(isOK:boolean)=>{
                                        if(isOK == false){
                                            this.init()
                                        }
                                    });
                                }
                                else{
                                    this.init();
                                }
                                
                            });
                        }
                    }else{
                        console.log("Wrong json from server");
                    }
                    
                });
                
            });
        }
        else{
            console.log("something bad with app "+this.app_id)
        }
    }
}