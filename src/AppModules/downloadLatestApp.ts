import { ServerObject } from "../interfaces";
import { Network } from "../network";
import { platform } from "os";

export class DownloadLastestApp{
    constructor(obj:ServerObject,app_folder_path:string,cb:any){
        (async ()=>{
            const network = new Network(obj.type,obj.platform);
        /*try{
            await network.downloadFile(obj.appName,obj.filename,app_folder_path);
            setTimeout(()=>{
                cb(true);
            },1000);
            
        }
        catch(e){
            cb(false);
        }*/

        try{
            network.downloadFile(obj.appName,obj.filename,app_folder_path,(s:boolean)=>{
                setTimeout(()=>{
                    cb(s);
                },1000);
            });
        }
        catch(e){
            cb(false);
        }
        })();
        
        

        
    }
}