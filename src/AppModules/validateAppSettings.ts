import { CheckingResultedObjects, LocalObject, ServerObject } from "../interfaces";
import { Network } from "../network";
import { Settings } from "../settings";
import { unlink } from "fs";
import { join } from "path";

export class ValidateAppSettings{
    constructor(id:string,settings:Settings,a_folder_path:string,cb:any){
        const makeCallback = (obj_old:LocalObject,obj_new:LocalObject,needUpdate:boolean,canContinue:boolean) =>{
            const o:CheckingResultedObjects={
                old:obj_old,
                new:obj_new,
                needUpdate:needUpdate,
                canContinue:canContinue
            }
            cb(o);
        }
        const network = new Network();
        (async ()=>{
            const server_json:ServerObject = await network.getLatestJsonDual(id);
            if(server_json != null){
                const new_json:LocalObject = {
                    keepRunning:false,
                    appName:server_json.appName,
                    filename:server_json.filename,
                    platform:server_json.platform,
                    timestamp:server_json.timestamp,
                    type:server_json.type,
                    version:server_json.version
                }
                if(settings.hasSetting("apps."+id)){
                    const current_json:LocalObject = settings.getSetting("apps."+id);
                    if(current_json.timestamp>=new_json.timestamp){
                        makeCallback(current_json,new_json,false,true);
                    }else{
                        try{
                            unlink(join(a_folder_path,current_json.filename),(err)=>{
                                if(err){}
                               
                                makeCallback(current_json,new_json,true,true);
                            });
                        }
                        catch(er){
                            console.error(er)
                            makeCallback(current_json,new_json,true,true);
                        }
                        
                        
                    }
                }else{
                    makeCallback(null,new_json,true,true);
                }
            }else{
                makeCallback(null,null,true,false);
            }
            
        })();
        
        
    }
}
