import {join} from "path";
import { Types } from "../enums";
import { LocalObject } from "../interfaces";
import { accessSync, mkdir, readFile } from "fs";
import * as rimraf from "rimraf";
import * as AdmZip from "adm-zip";
import * as child_process from 'child_process';
import * as os from "os";
import { LINE } from "../line";
import { unlink } from "fs";
export class RunApp{
    workDir:string="";
    cmd:string="";
    obj:LocalObject;
    constructor(obj:LocalObject,a_folder_path:string,main_cb){
        this.obj = obj;
        this.workDir = a_folder_path;
        console.log("workDir = ",this.workDir);
        switch(obj.type){
            case Types.AppImage:
            case Types.ARM32:
            case Types.ARM64:
            case Types.RUN:
            case Types.EXE:
                console.log("EXEC file")
                const filepath_exec = join(this.workDir,obj.filename);
                
                console.log("filepath_exec = ",filepath_exec);
                this.makePerm(this.workDir,obj.filename,()=>{
                    this.cmd =  (os.platform() == "win32"?".\\":"./")+'"' + obj.filename + '"';
                    if(this.isExist(filepath_exec)){
                        this.execute();
                        main_cb(true);
                    }   
                    else{
                        main_cb(false);
                    }
                });
                
            break;
            case Types.EXEC_ZIP:
                const filepath_zip = join(this.workDir,obj.filename);
                const binpath = join(this.workDir,"bin");
                const runWithEnvFile = () =>{
                    const envFile = join(binpath,"env.json");
                    if(this.isExist(envFile) == true){
                        readFile(join(binpath,"env.json"), (err:any, data:any) => {
                            if(err){
                                main_cb(false);
                            }
                            else{
                                try{
                                    const jsonOBJ = JSON.parse(data);
                                    const execFile = join(binpath,jsonOBJ.exec);
                                    this.makePerm(binpath,jsonOBJ.exec,()=>{
                                        this.cmd =  (os.platform() == "win32"?".\\":"./")+'"bin'+(os.platform() == "win32"?"\\":"/") + jsonOBJ.exec + '"';
                                        if(this.isExist(execFile) == true){
                                            if(this.isExist(filepath_zip) == true){
                                                unlink(filepath_zip,(err)=>{
                                                    if(err){}
                                                    this.execute();
                                                    main_cb(true);
                                                });
                                            }
                                            else{
                                                this.execute();
                                                main_cb(true);
                                            }
                                            
                                            
                                        }
                                        else{
                                            main_cb(false);
                                        }
                                    });
                                    
                                    
                                }
                                catch(eee){
                                    main_cb(false);
                                }
                                
                            }
                        })
                    }
                    else{
                        main_cb(false);
                    }
                }

                if(this.isExist(filepath_zip)){
                    rimraf(binpath,(err)=>{
                        if(!this.isExist(binpath)){
                            mkdir(binpath,()=>{
                                this.unzip(filepath_zip,binpath,()=>{
                                    runWithEnvFile();
                                });
                            });
                        }
                    });
                }
                else if(this.isExist(binpath) == true){
                    runWithEnvFile();
                }
                else{
                    main_cb(false);
                }
            break;
        }
    }

    isExist(filepath){
        try {
            accessSync(filepath);
            return true;
          } catch (err) {
            return false;
          }
    }

    execute(){
        
        child_process.exec(this.cmd,{
            cwd: this.workDir
          },  (err:any, stdout, stderr)=> {
            if (err) {
                console.error(err);
                console.log(LINE);
                return;
            }
            
            //console.log(stdout);
            //process.exit(0);// exit process once it is opened
            if(this.obj.keepRunning){
                console.log(LINE);
                this.execute()
            }
            else{
                
            } 
            
        });
    }

    makePerm(dir,filename,cb){
        if(os.platform() != "win32"){
            const cmd = "chmod +x "+filename;
            child_process.exec(cmd,{
                cwd: dir
            },  (err:any, stdout, stderr)=> {
                if (err) {
                    console.error(err);
                    console.log(LINE);
                    return;
                }
                cb();
                
            });
        }
        else{
            cb();
        }
        
    }

    unzip(zipfilepath,target,cb){
        (async ()=>{
            var zip = await new AdmZip(zipfilepath);
            await zip.extractAllTo(target, /*overwrite*/ true);
            cb();
        })();
    }

}
