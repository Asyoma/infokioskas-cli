import { AppFolder } from "../appFolder";
import { join } from "path";
import { existsSync,mkdirSync } from "fs";
export class CheckAppFolder{
    constructor(id:string,cb:any){
        const after = () =>{
            appFolder.getAppsPath()
            const a_path = join(appFolder.getAppsPath(),id);
            if(!existsSync(a_path)){
                mkdirSync(a_path,{recursive: true});
                cb(false,a_path);
            }
            else{
                cb(true,a_path);
            }
        }
        const appFolder = new AppFolder(()=>{
            after();
        });


    }
}