import {Main} from "./main";
import { Params } from "./interfaces";
import { Platforms, Types } from "./enums";
import {LINE, LINE_2}from "./line";
class Index{

    params:Params = {
        platform:Platforms.NULL,
        type:Types.NULL,
        list:false,
        add:{
            status:false,
            param:null
        },
        remove:{
            status:false,
            param:null
        }
    }
    constructor(){
        process.argv.forEach( (val, index, array)=> {
            switch(val){
                case "--help":
                    console.log(LINE);
                    console.log("Commands list:");
                    console.log(" --help")
                    console.log(" --list");
                    console.log(LINE_2);
                    console.log("Add/Remove apps by ID using:");
                    console.log(" --add <app id>");
                    console.log(" --remove <app id>");
                    console.log(LINE_2);
                    console.log("Types list:");
                    console.log(" --appimage");
                    console.log(" --arm32");
                    console.log(" --arm64");
                    console.log(" --exe");
                    console.log(LINE_2);
                    console.log("Platforms list:");
                    console.log(" --windows");
                    console.log(" --linux");
                    console.log(LINE);
                    process.exit(0);
                break;
                case "--list":
                    this.params.list = true;
                break;
                case "--appimage":
                    this.params.type = Types.AppImage;
                break;
                case "--arm32":
                    this.params.type = Types.ARM32;
                break;
                case "--arm64":
                    this.params.type = Types.ARM64;
                break;
                case "--exe":
                    this.params.type = Types.EXE;
                break;
                case "--windows":
                    this.params.platform = Platforms.WINDOWS;
                break;
                case "--linux":
                    this.params.platform = Platforms.LINUX;
                break;
                case "--add":
                    try{
                        this.params.add.status = true;
                        this.params.add.param = process.argv[index+1];
                    }
                    catch(e){
                        console.log("Wrong input. Type --help for more information.")
                        process.exit(0);
                    }
                
                break;
                case "--remove":
                    try{
                        this.params.remove.status = true;
                        this.params.remove.param = process.argv[index+1];
                    }
                    catch(e){
                        console.log("Wrong input. Type --help for more information.")
                        process.exit(0);
                    }
                    
                break;
            }
            if(index == process.argv.length-1){
                new Main(this.params); 
            }
        });
    }
}

new Index();