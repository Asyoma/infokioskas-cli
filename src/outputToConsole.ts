import { remove } from "lodash";
import { AppFolder } from "./appFolder";
import { Params } from "./interfaces";
import { LINE } from "./line";
import { Network } from "./network";
import { Settings } from "./settings";

export class OutputToConsole{
    params_obj:Params;
    appFolder:AppFolder;
    network:Network;
    settings:Settings;
    constructor(settings,params_obj:Params,cb){
        this.params_obj = params_obj;
        this.network = new Network(params_obj.type,params_obj.platform);
        this.settings = settings;
        
        cb();
    }
    listApps(cb){
        (async ()=>{
            const apps_list = await this.network.getAppsList();
            console.log(LINE);
            console.log("Apps list:");
            console.log(LINE);
            apps_list.forEach((app_id:string)=>{
                console.log("  -  "+app_id);
                if(apps_list.indexOf(app_id) == apps_list.length-1){
                    console.log(LINE);
                    console.log("End of apps list.");
                    console.log(LINE);
                   cb();
                }
            });
        })();

    }
    addAndRemove(cb){
        console.log(LINE);
        console.log("That can't be done!!!");
        console.log(LINE);
        cb();

    }
    addApp(cb){
        (async ()=>{
            const apps_list = await this.network.getAppsList();
            if(apps_list.includes(this.params_obj.add.param) && !this.settings.getSetting("list").includes(this.params_obj.add.param)){
                this.settings.addValue("list",this.params_obj.add.param);
                console.log(LINE);
                console.log('Added app "'+this.params_obj.add.param+'"');
                cb();
            }
            else{
                console.log(LINE);
                console.log("Can't add app "+'"'+this.params_obj.add.param+'"');
                cb();
            }
        })();
        
        

    }
    removeApp(cb){
        let currentList = this.settings.getSetting("list");
        if(currentList.includes(this.params_obj.remove.param)){
            let newList = remove(currentList, (item) => {
                return item !=this.params_obj.remove.param;
            });
            this.settings.setSetting("list",newList);
            console.log(LINE);
            console.log('Removed app "'+this.params_obj.remove.param+'"');
            cb();
        }
        else{
            console.log(LINE);
            console.log('Nothing to remove');
            cb();
        }

    }
}