import * as path from "path";
const default_settings:any={
    list:["its-kiosk-browser"]
}
export class Settings{
    store:any=null;
    appFolder;
    constructor(appFolder:any,cb:any){
        this.appFolder = appFolder;
        this.store = require('data-store')({ path: path.join(this.appFolder,'settings.json') }); 
        setTimeout(()=>{
            cb();
        },100)
        
        
    }

    getSetting(key:string){
        if(this.store.has(key)){
            return this.store.get(key);
        }
        else{
            return null;
        }
        
    }

    addValue(key:string,value:string){
        this.store.union(key,value);
    }
    
    setSetting(key:string,obj:any){
        this.store.set(key,obj);
    }

    hasSetting(key:string){
        try{
            return this.store.has(key);
        }
        catch(e){
            return false;
        }
    }
}